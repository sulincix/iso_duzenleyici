#!/bin/bash
mkdir /mnt/iso/ 
cd ../ 
if [ -f config/iso ]
then
  export iso="$(cat config/iso)"
else
  echo "$(cat malzeme/$(cat config/lang)/6)"
  read iso
  if [ -f "$iso" ]
  then
    echo "$iso" | sed "s|file://||g" > ./config/iso
  else
    echo "$(cat malzeme/$(cat config/lang)/12)"
  fi
fi

#iso=$(zenity -file-selection)
umount /mnt/iso
mount  $iso /mnt/iso/ 
mkdir /mnt/squashfs/ 
if [ -f /mnt/iso/live/filesystem.squashfs ]
then
  squashfs=/mnt/iso/live/filesystem.squashfs
elif [ -f /mnt/iso/casper/filesystem.squashfs ]
then
  squashfs=/mnt/iso/casper/filesystem.squashfs
else
  echo "$(cat malzeme/$(cat config/lang)/7)" 
  read squashfs
fi

rm -rf chroot
unsquashfs $squashfs
mv squashfs-root chroot 
echo "nameserver 8.8.8.8" > ./chroot/etc/resolv.conf
mount --bind /dev/ ./chroot/dev/ 2> /dev/null
mount --bind /sys/ ./chroot/sys/ 2> /dev/null
mount --bind /proc/ ./chroot/proc/ 2> /dev/null
mount --bind /run/ ./chroot/run/ 2> /dev/null
mount --bind /dev/pts/ ./chroot/dev/pts/ 2> /dev/null
