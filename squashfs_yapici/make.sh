#!/bin/bash
export konum=$(pwd)
cd ..
if [ -f ./config/chroot ]
then
  export chrootx=$(cat ./config/chroot)
else
  echo "$(cat malzeme/$(cat config/lang)/14)"
  exit 1
fi

rm -rf $chrootx/var/log/*
rm -rf $chrootx/var/cache/apt/archives/*
cd $chrootx/root/
rm -rf ./{*,.*} 2> /dev/null
cp -prf $chrootx/etc/skel/ -T $chrootx/root/
rm -rf $chrootx/var/lib/apt/lists/*
cd $chrootx/tmp/
rm -rf ./{*,.*} 2> /dev/null
cd $konum
umount -Rf $chrootx/dev/pts/ 2> /dev/null
umount -Rf $chrootx/proc/ 2> /dev/null
umount -Rf $chrootx/sys/ 2> /dev/null
umount -Rf $chrootx/dev/ 2> /dev/null
umount -Rf $chrootx/run/ 2> /dev/null
mksquashfs $chrootx ./filesystem.squashfs -comp xz -wildcards
mount --bind /dev/ $chrootx/dev/ 2> /dev/null
mount --bind /sys/ $chrootx/sys/ 2> /dev/null
mount --bind /proc/ $chrootx/proc/ 2> /dev/null
mount --bind /run/ $chrootx/run/ 2> /dev/null
mount --bind /sys/ $chrootx/dev/pts/ 2> /dev/null

